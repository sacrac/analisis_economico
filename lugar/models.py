# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible


from django.db import models
from django.template import defaultfilters

# Create your models here.

@python_2_unicode_compatible
class Pais(models.Model):
    nombre = models.CharField(max_length=200)
    slug = models.SlugField(unique=True, editable=False)
    latitud = models.DecimalField('Latitud', max_digits=8, decimal_places=5, blank=True, null=True)
    longitud = models.DecimalField('Longitud', max_digits=8, decimal_places=5, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Países"
        unique_together = ('nombre',)

    def save(self, *args, **kwargs):
      self.slug = defaultfilters.slugify(self.nombre)
      super(Pais, self).save(*args, **kwargs)

    def __str__(self):
        return self.nombre

@python_2_unicode_compatible
class Departamento(models.Model):
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=30, unique=True)
    slug = models.SlugField(unique=True, editable=False)
    latitud = models.DecimalField('Latitud', max_digits=8, decimal_places=5, blank=True, null=True)
    longitud = models.DecimalField('Longitud', max_digits=8, decimal_places=5, blank=True, null=True)

    class Meta:
        verbose_name = 'Departamento/Provincia'
        verbose_name_plural = "Departamentos/Provincias"
        unique_together = ('nombre',)
        ordering = ['nombre']

    def save(self, *args, **kwargs):
      self.slug = defaultfilters.slugify(self.nombre)
      super(Departamento, self).save(*args, **kwargs)

    def __str__(self):
        return self.nombre

@python_2_unicode_compatible
class Municipio(models.Model):
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=130, unique=True)
    slug = models.SlugField(max_length=130, unique=True, editable=False)
    latitud = models.DecimalField('Latitud', max_digits=8, decimal_places=5, blank=True, null=True)
    longitud = models.DecimalField('Longitud', max_digits=8, decimal_places=5, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Municipios"
        unique_together = ('nombre','departamento',)
        ordering = ['departamento__nombre', 'nombre']

    def save(self, *args, **kwargs):
      self.slug = defaultfilters.slugify(self.nombre)
      super(Municipio, self).save(*args, **kwargs)

    def __str__(self):
        return '%s - %s' % (self.departamento.nombre, self.nombre)

@python_2_unicode_compatible
class Comunidad(models.Model):
    municipio = models.ForeignKey(Municipio, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=140)
    slug = models.SlugField(max_length=140, editable=False)
    latitud = models.DecimalField('Latitud', max_digits=8, decimal_places=5, blank=True, null=True)
    longitud = models.DecimalField('Longitud', max_digits=8, decimal_places=5, blank=True, null=True)

    class Meta:
        verbose_name_plural="Comunidades"
        unique_together = ('nombre','municipio',)
        ordering = ['nombre']

    def save(self, *args, **kwargs):
      self.slug = defaultfilters.slugify(self.nombre)
      super(Comunidad, self).save(*args, **kwargs)

    def __str__(self):
        return u'%s' % self.nombre
