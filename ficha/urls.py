from django.conf.urls import url
from .views import *
from django.views.generic import TemplateView

urlpatterns =  [
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    #url(r'^consultar/', consulta_herramienta, name='linea-consulta'),
    #url(r'^consultar-establecimiento/', consulta_establecimiento, name='consulta-establecimiento'),
    url(r'^entrevistados-autocomplete/',
        EntrevistadosAutocomplete.as_view(),
        name='entrevistados-autocomplete',),
    url(
        r'^departamento-autocomplete/',
        DepartamentoAutocomplete.as_view(create_field='nombre'),
        name='depart-autocomplete',
    ),
    url(
        r'^municipio-autocomplete/',
        MunicipioAutocomplete.as_view(create_field='nombre'),
        name='municipio-autocomplete',
    ),
    url(
        r'^comunidad-autocomplete/',
        ComunidadAutocomplete.as_view(create_field='nombre'),
        name='comunidad-autocomplete',
    ),
    url(r'^consultar/', consulta_ficha, name='linea-consulta'),
    url(r'^api/productor/', get_productor, name='productor-search'),
    url(r'^ajax/load-departamentos/', load_departamentos, name='ajax_load_departamentos'),
    url(r'^ajax/load-municipios/', load_municipios, name='ajax_load_municipios'),
    url(r'^ajax/load-comunidad/', load_comunidades, name='ajax_load_comunidad'),
    #url(r'^mapaherramienta/', obtener_lista_mapa_cacao, name='obtener-lista-mapa-cacao'),
    url(r'^(?P<vista>\w+)/$', get_view, name='get-view'),
]
