# -*- coding: utf-8 -*-

CHOICE_SEXO = (
                (1, 'Mujer'),
                (2, 'Hombre'),
              )

CHOICE_MANEJA_FINCA = (
    (1, 'Hombre'),
    (2, 'Mujer'),
    (3, 'Ambos'),
    (4, 'Entre varios')
)

CHOICE_VALORACION_SUELO = (
    (1, 'Óptimo'),
    (2, 'Medio óptimo'),
    (3, 'No óptimo'),
)

CHOICE_TIPO_MANEJO = (
    (1, 'Orgánico'),
    (2, 'Orgánico-químico'),
    (3, 'Químico'),
)

CHOICE_INTENSIDAD_MANEJO = (
    (1, 'No intensivo'),
    (2, 'Medio intensivo'),
    (3, 'Intensivo'),
)

CHOICE_TAMANO_PARCELA = (
    (1, 'Pequeña (0.1 a 2 mz)'),
    (2, 'Mediana (2.1 a 5 mz)'),
    (3, 'Grande (Más de 5 mz)'),
)

CHOICE_PENDIENTE = (
    (1, 'Plano'),
    (2, 'Ondulado'),
    (3, 'Con fuerte pendiente'),
)

CHOICE_TIPO_SISTEMA_AGROFORESTAL = (
    (1, 'SAF Cacao Sencillo'),
    (2, 'SAF Cacao Diversificado'),
    (3, 'SAF Cacao + Café Sencillo'),
    (4, 'SAF Cacao + Café Diversificado'),
)


CHOICE_DESCRIPCION_SISTEMA_AGRO = (
    (1, 'Solo Cacao'),
    (2, 'Cacao + Granos básicos'),
    (3, 'Cacao + Musaceas'),
    (4, 'Cacao + Musaceas + Granos básicos'),
    (5, 'Cacao + Maderables'),
    (6, 'Cacao + Granos básicos + Maderables'),
    (7, 'Cacao + Musaceas + Maderables'),
    (8, 'Cacao + Musaceas + Granos básicos + Maderables '),
    (9, 'Cacao + Frutales '),
    (10, 'Cacao + Frutales + Maderables'),
     (11, 'Cacao + Frutales + Granos básicos'),
    (12, 'Cacao + Frutales + Granos básicos + Maderables '),
    (13, 'Cacao + Frutales + Musaceas'),
    (14, 'Cacao + Frutales + Musaceas + Maderables'),
    (15, 'Cacao + Frutales + Musaceas + Granos básicos '),
    (16, 'Cacao + Frutales + Musaceas + Granos básicos + Maderables '),
    (17, 'Cacao + Café'),
    (18, 'Cacao + Café + Granos básicos '),
    (19, 'Cacao + Café + Musaceas '),
    (20, 'Cacao + Café + Maderables '),
     (21, 'Cacao + Café + Musaceas + Maderables '),
    (22, 'Cacao + Café + Musaceas + Granos básicos '),
    (23, 'Cacao + Café + Frutales '),
    (24, 'Cacao + Café + Frutales + Maderables '),
    (25, 'Cacao + Café + Frutales + Granos básicos '),
    (26, 'Cacao + Café + Frutales + Granos básicos + Maderables '),
    (27, 'Cacao + Café + Frutales + Musaceas '),
    (28, 'Cacao + Café + Frutales + Musaceas + Maderables '),
    (29, 'Cacao + Café + Frutales + Musaceas + Granos básicos'),
    (30, 'Cacao + Café + Frutales + Musaceas + Granos básicos + Maderables '),
)


CHOICE_RIEGO = (
    (1, 'No hay riego'),
    (2, 'Riego de baja intensidad '),
    (3, 'Riego de alta intensidad '),
)

CHOICE_COSTO_CACAO_ACTIVIDAD = (
    (1, 'Siembra'),
    (2, 'Resiembra'),
    (3, 'Transporte de materiales'),
    (4, 'Injertación de cacao'),
    (5, 'Poda de cacao'),
    (6, 'Deschuponado'),
    (7, 'Poda de árboles'),
    (8, 'Aplicación de abono'),
    (9, 'Aplicación de insecticida'),
    (10, 'Aplicación de fungicida'),
    (11, 'Transporte de insumos'),
    (12, 'Manejo de piso (maleza)'),
    (13, 'Aplicación de riego'),
    (14, 'Obras de drenaje'),
    (15, 'Obras de conservación'),
    (16, 'Cosecha y Corte'),
    (17, 'Secado o Fermento'),
    (18, 'Transporte de cosecha'),
    )


CHOICE_MONEDA_LOCAL = (
    (1, 'NIO'),
    (2, 'HNL'),
    (3, 'USD'),
    (4, 'DOP'),
    (5, 'GTQ'),
    )

CHOICE_UNIDAD_DATOS = (
    (1, 'qq'),
    (2, 'Cabeza'),
    (3, 'Lb'),
    (4, 'Kg'),
    (5, 'Docena'),
    (6, 'Cien'),
    (7, 'Saco'),
    (8, 'M3'),
    (9, 'Pt'),
    (10, 'Unidad'),
    (11, 'Carga'),
    (12, 'Racimo'),
    )

CHOICE_UNIDAD_TIERRA = (
    (1, 'Mz'),
    (2, 'Ha'),
    (3, 'Tarea DOM'),
    (4, 'Mt2'),
    (5, 'Tarea GUATE'),
  )

CHOICE_COMPRADOR_DATOS = (
    ('A', 'Intermediario'),
    ('B', 'Cooperativa'),
    ('C', 'Asociación'),
    ('D', 'Empresa'),
    )

CHOICE_PRODUCTOS_CACAO = (
    (1, 'Cacao en baba'),
    (2, 'Cacao seco fermentado'),
    (3, 'Cacao seco no fermentado'),
)

CHOICE_PRODUCTOS_MUSACEAS = (
    (1, 'Plátano'),
    (2, 'Banano'),
    (3, 'Guineo'),
)

CHOICE_COSTO_MUSACEAS_ACTIVIDAD = (
    (1, 'Siembra'),
    (2, 'Resiembra'),
    (3, 'Transporte de materiales'),
    (4, 'Deshije y deshoja'),
    (5, 'Manejo de cabezas'),
    (6, 'Aplicación de abono'),
    (7, 'Aplicación de insecticida'),
    (8, 'Aplicación de fungicida'),
    (9, 'Transporte de insumos'),
    (10, 'Cosecha y Corte'),
    (11, 'Transporte de cosecha'),
    )


CHOICE_PRODUCTOS_FRUTALES = (
    (1, 'Zapote'),
    (2, 'Cítricos'),
    (3, 'Aguacate'),
    (4, 'Coco'),
    (5, 'Mango'),
    (6, 'Nance'),
    (7, 'Marañon'),
    (8, 'Jocote'),
    (9, 'Nispero'),
    (10, 'Mamón'),
    (11, 'Guayaba'),
    (12, 'Anona'),
)

CHOICE_COSTO_FRUTALES_ACTIVIDAD = (
    (1, 'Siembra'),
    (2, 'Resiembra'),
    (3, 'Transporte de materiales'),
    (4, 'Poda de frutales'),
    (5, 'Aplicación de abono'),
    (6, 'Aplicación de insecticida'),
    (7, 'Aplicación de fungicida'),
    (8, 'Transporte de insumos'),
    (9, 'Cosecha y Corte'),
    (10, 'Transporte de cosecha'),
    )

CHOICE_COSTO_MADERABLES_ACTIVIDAD = (
    (1, 'Siembra'),
    (2, 'Resiembra'),
    (3, 'Transporte de materiales'),
    (4, 'Poda de maderables'),
    (5, 'Aplicación de abono'),
    (6, 'Aplicación de insecticida'),
    (7, 'Aplicación de fungicida'),
    (8, 'Transporte de insumos'),
    (9, 'Cosecha y Corte'),
    (10, 'Transporte de cosecha'),
    )

# CHOICE_PRODUCTOS_MADERABLES = (
#     (1, 'Madera-Caoba'),
#     (2, 'Madera-Cortez'),
#     (3, 'Madera-Roble'),
#     (4, 'Madera-Melina'),
#     (5, 'Madera-Granadillo'),
#     (6, 'Madera-Leña'),
# )

CHOICE_PRODUCTOS_MADERABLES = (
    (1, 'Madera fina'),
    (2, 'Madera no fina'),
    (3, 'Madera-leña'),
    (4, 'Bálsamo/Resina'),
    (5, 'Miel'),
)

CHOICE_COSTO_ARBOLES_ACTIVIDAD = (
    (1, 'Siembra'),
    (2, 'Resiembra'),
    (3, 'Transporte de materiales'),
    (4, 'Poda de árboles'),
    (5, 'Aplicación de abono'),
    (6, 'Aplicación de insecticida'),
    (7, 'Aplicación de fungicida'),
    (8, 'Transporte de insumos'),
    (9, 'Cosecha y Corte'),
    (10, 'Transporte de cosecha'),
    )

CHOICE_PRODUCTOS_ARBOLES = (
    (1, 'Gandul'),
    (2, 'Guaba'),
)

CHOICE_COSTO_GRANOS_ACTIVIDAD = (
    (1, 'Siembra'),
    (2, 'Resiembra'),
    (3, 'Transporte de materiales'),
    (4, 'Aporque'),
    (5, 'Limpieza de malezas'),
    (6, 'Aplicación de abono'),
    (7, 'Aplicación de insecticida'),
    (8, 'Aplicación de fungicida'),
    (9, 'Aplicación de herbicida'),
    (10, 'Transporte de insumos'),
    (11, 'Cosecha y Corte'),
    (12, 'Transporte de cosecha'),
    )

CHOICE_PRODUCTOS_GRANOS = (
    (1, 'Maíz'),
    (2, 'Frijol'),
)

CHOICE_COSTO_CAFE_ACTIVIDAD = (
    (1, 'Siembra'),
    (2, 'Resiembra'),
    (3, 'Transporte de materiales'),
    (4, 'Poda de café'),
    (5, 'Poda de árboles'),
    (6, 'Aplicación de abono'),
    (7, 'Aplicación de insecticida'),
    (8, 'Aplicación de fungicida'),
    (9, 'Transporte de insumos'),
    (10, 'Manejo de piso (maleza)'),
    (11, 'Aplicación de riego'),
    (12, 'Obras de drenaje'),
    (13, 'Obras de conservación'),
    (14, 'Cosecha y Corte'),
    (15, 'Despulpado y fermento'),
    (16, 'Secado'),
    (17, 'Transporte de cosecha'),
    )

CHOICE_PRODUCTOS_CAFE = (
    (1, 'Café en pergamino'),
    (2, 'Café en Uva'),
)
