# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.forms import SelectMultiple
from django.db import models
from .models import LineaTiempoCostos, DatosCosto, LineaTiempoCosecha, DatosCosecha
from .forms import ConfiguraionForm
# Register your models here.

class DatosCostoInlines(admin.TabularInline):
    model = DatosCosto
    extra = 1

class LineaTiempoAdmin(admin.ModelAdmin):
    form = ConfiguraionForm
    inlines = [DatosCostoInlines]

# class DatosCosechaInlines(admin.TabularInline):
#     model = DatosCosecha
#     extra = 1

# class LineaTiempoCosechaAdmin(admin.ModelAdmin):
#     inlines = [DatosCosechaInlines]

admin.site.register(LineaTiempoCostos, LineaTiempoAdmin)
#admin.site.register(LineaTiempoCosecha, LineaTiempoCosechaAdmin)
