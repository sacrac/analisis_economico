# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from lugar.models import Pais, Departamento, Municipio

# Create your models here.
CHOICES_ANIO = (
        (1, 'Año 1'),
        (2, 'Año 2'),
        (3, 'Año 3'),
        (4, 'Año 4'),
        (5, 'Año 5'),
        (6, 'Año 6'),
        (7, 'Año 7'),
        (8, 'Año 8'),
        (9, 'Año 9'),
        (10, 'Año 10'),
        (11, 'Año 11'),
        (12, 'Año 12'),
        (13, 'Año 13'),
        (14, 'Año 14'),
        (15, 'Año 15'),
        (16, 'Año 16'),
        (17, 'Año 17'),
        (18, 'Año 18'),
        (19, 'Año 19'),
        (20, 'Año 20'),
        (21, 'Año 21'),
        (22, 'Año 22'),
        (23, 'Año 23'),
        (24, 'Año 24'),
        (25, 'Año 25'),
    )

CHOICES_CULTIVOS_ESTADO = (
            (1, 'Establecimiento'),
            (2, 'Desarrollo'),
            (3, 'Producción'),
            (4, 'No tiene'),
    )
class LineaTiempoCostos(models.Model):
    fecha = models.DateField(null=True)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    departamento = models.ManyToManyField(Departamento)

    def __unicode__(self):
        return '%s - %s' % (self.fecha, self.pais)

    class Meta:
        verbose_name_plural = 'Línea de tiempo de cultivos para cálculo de rentabilidad'

class DatosCosto(models.Model):
    costo = models.ForeignKey(LineaTiempoCostos, on_delete=models.CASCADE)
    anio = models.IntegerField(choices=CHOICES_ANIO, verbose_name='Año')
    cacao = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    cafe = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    musaceas = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    frutales = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    maderables = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    arboles = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    granos = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)

    class Meta:
        verbose_name_plural = 'Calculos de rentabilidad'

class LineaTiempoCosecha(models.Model):
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    departamento = models.ManyToManyField(Departamento)

    def __unicode__(self):
        return 'Cultivo Cosecha'

    class Meta:
        verbose_name_plural = 'Línea de tiempo de cultivos para cálculo de Cosecha'

class DatosCosecha(models.Model):
    costo = models.ForeignKey(LineaTiempoCosecha, on_delete=models.CASCADE)
    anio = models.IntegerField(choices=CHOICES_ANIO, verbose_name='Año')
    cacao = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    cafe = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    musaceas = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    frutales = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    maderables = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    arboles = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    granos = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)

    class Meta:
        verbose_name_plural = 'Calculos de cosecha'
