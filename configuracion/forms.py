from django import forms

from .models import *

class ConfiguraionForm(forms.ModelForm):
    departamento = forms.ModelMultipleChoiceField(queryset=Departamento.objects.all(),
                                                                                widget=forms.CheckboxSelectMultiple(),
                                                                                required=False)
    class Meta:
        model = LineaTiempoCostos
        fields = "__all__"
